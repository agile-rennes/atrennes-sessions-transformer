'use strict';

const fs = require('fs');
const {BUILD_DIRECTORY, BUILD_FILENAME} = require("./config");

const orga_speaker = {
    "name": "Equipe d'organisation 2023",
    "photoUrl": "https://agiletour.agilerennes.org/images/logo.svg",
    "socials": [],
    "id": "311c8e54-6a6f-436b-886d-92f0e8978910"
}
const orga_room = {
    "id": "1c18ced0-5e52-413f-b838-94d787746e4a",
    "name": "Agile Tour Rennes 2023",
}
const orga_session_day_1 = {
    "speakers": [orga_speaker.id],
    "tags": [],
    "title": "Agile Tour Rennes 2023 - J1",
    "id": "ed2a2bfc-0896-4935-982a-74e034881cc0",
    "startTime": "2023-10-19T08:00:00",
    "endTime": "2023-10-19T18:00:00",
    "trackTitle": orga_room.name
}

const orga_session_day_2 = {
    "speakers": [orga_speaker.id],
    "tags": [],
    "title": "Agile Tour Rennes 2023 - J2",
    "id": "dfc15103-ece0-4c58-9b59-26a1dfbb42cb",
    "startTime": "2023-10-20T08:00:00",
    "endTime": "2023-10-20T17:30:00",
    "trackTitle": orga_room.name
}

const initBuild = () => {
    fs.rmSync(BUILD_DIRECTORY, {recursive: true, force: true});
    fs.mkdirSync(BUILD_DIRECTORY)
}

const transform = () => {
    initBuild()
    fetch(process.env.SESSIONIZE_API_URL)
        .then((response) => response.json())
        .then((originalData) => {
            const rooms = extractRooms(originalData)
            originalData.sessions = transformSessions(originalData, rooms)
            originalData.speakers = transformSpeakers(originalData)
            let data = JSON.stringify(originalData);
            fs.writeFileSync(BUILD_DIRECTORY + "/" + BUILD_FILENAME, data);
        });
}

const extractRooms = (originalData) => {
    const rooms = {}
    originalData.rooms.push(orga_room)
    originalData.rooms.forEach(room => rooms[room.id] = room.name)
    return rooms
}

const transformSessions = (originalData, rooms) => {
    const transformedSessions = {}
    transformedSessions[orga_session_day_1.id] = orga_session_day_1
    transformedSessions[orga_session_day_2.id] = orga_session_day_2
    originalData.sessions.forEach(session => {
        transformedSessions[session.id] = {
            "speakers": session.speakers,
            "tags": [],
            "title": session.title,
            "id": session.id,
            "startTime": session.startsAt,
            "endTime": session.endsAt,
            "trackTitle": rooms[session.roomId]
        }
    })
    return transformedSessions
}

const transformSpeakers = (originalData) => {
    const transformedSpeakers = {}
    transformedSpeakers[orga_speaker.id] = orga_speaker
    originalData.speakers.forEach(speaker => {
        transformedSpeakers[speaker.id] = {
            "name": speaker.fullName,
            "photoUrl": speaker.profilePicture,
            "socials": [],
            "id": speaker.id
        }
    })
    return transformedSpeakers
}

transform()